console.log("hi");

console.log(document);
//result: the document HTML code

const txtFirstName = document.querySelector("#txt-first-name");

console.log(txtFirstName);
// result: the input field

/*
	alternative ways:
		>> document.getElementById("txt-first-name")
		>> document.getElementByClassName("text-class")
		>> document.getElementByTagName("h1")
*/

// targets the full name
let spanFullName = document.querySelector("#span-full-name");

// event listeners
// (event) can have a shorthand of (e)
// addEventListener('stringOfAnEvent', function)
// innerHTML allows us to call the tag and change the value dynamically
// Each HTML element has an innerHTML property that defines both the HTML code and the text that occurs between that element's opening and closing tag. By changing an element's innerHTML after some user interaction, you can make much more interactive pages.

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value
});

// multiple listeners
// The function being called when an event is triggered will receive an argument called event.
// The event argument contains the information on the triggered event.
// The event.target contains the element where the event happened and event.target.value gets the value of the input object (similar to the txtFirstName.value).
txtFirstName.addEventListener('keyup', (event) => {
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
});

// ACTIVITY

// const updateFullName = () => {

// 	let firstName = txtFirstName.value;
// 	let lastName = txtLastName.value;

// 	spanFullName.innerHTML = firstName + lastName

// }

// txtLastName.addEventListener('keyup', updateFullName);
// txtFirstName.addEventListener('keyup', updateFullName)